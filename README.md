**react-native-textinput-material-autocomplete**
This is a package based on [react-native-paper](https://www.npmjs.com/package/react-native-paper) textinput material, this component allows you to filter objects from an array by typing in the text field
![](https://media.giphy.com/media/KyGi4bkAQsDtdX7tqX/giphy.gif)

## Usage

##### [](https://www.npmjs.com/package/react-native-dropdown-autocomplete#install-the-package)Install the package

```
npm i react-native-textinput-material-autocomplete
```
```
import * as React  from  'react';

import { Text, View, StyleSheet} from  'react-native';

import {marcas} from  "./utilities/marcas.json"

import {colores} from  "./utilities/colores.json"

import  Autocomplete  from  "react-native-textinput-material-autocomplete"

export  default  class  FormCar  extends  React.Component {

constructor(props){

super(props)

this.state={}

}

render() {

return (

<View style={styles.container}>

<View style={{}}>

<View style={[styles.containerFields,{zIndex:6}]}>

<Autocomplete array={marcas} field="nombre" label="Brand" value={(val) => { }} error={() => {console.log("field invalid") }} > </Autocomplete>

</View >

<View style={[styles.containerFields,{zIndex:5}]}>

<Autocomplete error={() => {console.log("field invalid") }} array={colores} field="nombre" label="Color" value={(val) => { }} > </Autocomplete>

</View>

</View>

</View>

);

}

}

  

const styles = StyleSheet.create({

container: {

flex: 1,

paddingTop:80,

paddingHorizontal:30

},

containerFields:{

marginBottom:10

}

});
```